import { createSwitchNavigator,  } from 'react-navigation';
import { createStackNavigator,TransitionPresets } from 'react-navigation-stack';
import Home from '../component/home';
import Detail from '../component/detail';

const navOptionHandler = navigation => ({
  headerShown: false,
});

const MainStack = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: navOptionHandler
  },
  Detail: {
    screen: Detail,
    navigationOptions: navOptionHandler
  }
},
{
  defaultNavigationOptions:{
    ...TransitionPresets.SlideFromRightIOS,
  }
});


const Routes =  createSwitchNavigator(
    {
        Main: MainStack,
    },
    {
      initialRouteName: 'Main',
    }
);

  
export default Routes;
