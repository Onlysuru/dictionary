import SQLite from 'react-native-sqlite-storage';
var db = {};

const makeSqlConnection = ()=>{
    db = SQLite.openDatabase(
    {
        name: 'sqlite',
        location: 'default',
        createFromLocation: '~sqlite.db',
    },
    ()=>{console.log('success')},  //okCallback
    ()=>{console.log('fail')}// error callback
    );
}


/**

* Select Query Example

*/

exports.QueryExecute = async(sql,params)=>{
    if(Object.keys(db).length == 0)
    {
        makeSqlConnection();
    }
        return new Promise((resolve, reject) => {

            db.transaction((trans) => {
        
                trans.executeSql(sql, params, (trans, results) => {
                    resolve(results);
                },
        
                (error) => {
                reject(error);
        
                });
        
            });
        
        });
    
}