import React ,{Component}from 'react';
import { StyleSheet,TextInput,ActivityIndicator,Modal,FlatList,ScrollView,Switch,TouchableOpacity, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as keywords from '../containt/words';
import Realm from 'realm';

export default class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
            keywords:[],
            offline:true,
            realm: null,
            isLoading:false
        }        
    }
  
    componentDidMount(){
        this.getKeywords();
    }
    
    loadDataBase = () =>{
        keywords.default.map((value,index)=>{
            Realm.open({
                schema: [{name: 'dictionary', properties: {title:'string',description:'string'}}]
              }).then(realm => {
                realm.write(() => {
                    realm.create('dictionary', {title:value,description:value});
                });
            });

            if(keywords.default.length == index - 1)
            {
                this.getKeywords();
            }
        })
    }

    getKeywords = async ()=>{
        let realm = await Realm.open({
            schema: [{name: 'dictionary', properties: {title:'string',description:'string'}}]
          });

        if(realm.objects('dictionary').length > 0)
        {
            this.setState({
                keywords:realm.objects('dictionary'),
                realm:realm,
                isLoading:false
            });
        }
        else
        {
            this.setState({isLoading:true});
            this.loadDataBase();
        }
    }


    searchWords = async(word) =>{
        if (word.length > 0 && this.state.realm.objects('dictionary').length > 0) {
            this.setState({isLoading:true});
            const data = this.state.realm.objects('dictionary').filtered(`title Like[c] "${word}*"`);
            this.setState({
                keywords:data,
                isLoading:false
            });
        }
        else
        {
            this.getKeywords();
        }
    }

    render(){
        const {isLoading} = this.state;
        return(
            <View style={{flex:1}}>
               
                <View style={{backgroundColor:'#0f436d',height:140}}>
                    <View>
                        <Text style={{fontSize:28,fontWeight:'bold',color:'white',paddingLeft:20}}>
                            Search
                        </Text>
                    </View>
                    <View style={styles.searchBox}>
                        <View style={{borderRadius:5,paddingHorizontal:8,flexDirection:'row',backgroundColor:'white'}}>
                            <Icon name="search" style={{fontSize: 22,marginTop:8,color: 'gray'}} /> 
                            <TextInput onChangeText={(value)=>{this.searchWords(value)}} placeholder="Search Dictionary..." style={{fontSize:16,width:'96%',height:40}}/>
                        </View>
                    </View>
                    <View style={styles.offlineContainer}>
                        <View>
                            <Text style={styles.offlineText}> 
                                <Icon name="plane" style={{fontSize: 22,color: 'white'}} /> Offline Mode 
                            </Text>
                        </View>
                        <View style={{flex:1,alignSelf:'flex-end'}}>
                            <Switch
                                trackColor={{ false: "gray", true: "lightgreen" }}
                                thumbColor={this.state.offline ? "white" : "white"}
                                ios_backgroundColor="#3e3e3e"
                                onValueChange={()=>{
                                    this.setState({
                                        offline:!this.state.offline
                                    })
                                }}
                                value={this.state.offline}
                            /> 
                            
                        </View>
                    </View>
                </View>
                <ScrollView>
                    <View style={{backgroundColor:'lightgray'}}>
                        <Text style={{textAlign:'center',paddingVertical:5,color:'#0f436d'}}> 
                            SUGGESTION
                        </Text>
                    </View>
                    <Modal
                        transparent={true}
                        animationType={'fade'}
                        visible={isLoading}>
                        <View style={{paddingTop:'80%',paddingLeft:'35%'}}>
                            <View style={styles.activityIndicatorWrapper}>
                                <ActivityIndicator
                                animating={isLoading} color='red' />
                                <Text>Loading...</Text>
                            </View>
                        </View>
                    </Modal> 
                    <View>
                     
                        <FlatList
                            data={this.state.keywords}
                            keyExtractor={(item,index)=>{index}}
                            renderItem={({item,index})=>(
                                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Detail',{keyword:item.title})}}>
                                    <Text style={{paddingVertical:10,paddingLeft:10,color:'black',fontSize:17,fontWeight:'600'}}> 
                                        {item.title}
                                    </Text>
                                </TouchableOpacity>
                            )}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }

}


  

const styles = StyleSheet.create({
    offlineText:{
        color:'white',
        fontSize:16,
        marginTop:5
    },
    offlineContainer:{
        borderTopColor: 'white',
        borderTopWidth: 0.2,
        marginTop:'1%',
        width:'90%',
        alignSelf:'center',
        flexDirection:'row'
    },
    searchBox:{
        alignSelf:'center',
        width:'90%',
        marginTop:"2%",
        height:'40%'
    },
    suggestionContainer:{
        color:'darkblue',
        marginTop:'1%',
        marginBottom:'1%',
        textAlign:'center'
    },
    
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: 'white'
    },
    activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        height: 60,
        width: 120,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    }
});


    