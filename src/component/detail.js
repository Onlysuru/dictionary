import React ,{Component}from 'react';
import {TouchableWithoutFeedback, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Realm from 'realm';

export default class Detail extends Component{
    constructor(props){
        super(props);
        this.state = {
            detail:''
        }
        let keyword = this.props.navigation.state.params.keyword;
        
        this.getDetails(keyword);
    }
  
    getDetails = async (keyword)=>{
        let realm = await Realm.open({
            schema: [{name: 'dictionary', properties: {title:'string',description:'string'}}]
          });
          
        if (keyword.length > 0 && realm.objects('dictionary').length > 0) {
            const data = realm.objects('dictionary').filtered(`title = "${keyword}"`);
            if(data.length > 0)
            {
                this.setState({
                    detail:data[0].description
                });
            }
            
        }
    }

    render(){
        return(
            <View style={{flex:1}}>
                <View style={{backgroundColor:'#0f436d',height:'8%'}}>
                    <TouchableWithoutFeedback onPress={()=>{this.props.navigation.goBack()}}>
                        <Icon name="arrow-left" style={{fontSize: 28,marginTop:'3%',marginLeft:'3%',color: 'white'}} /> 
                    </TouchableWithoutFeedback>
                </View>
                <View style={{padding:15}}>
                    <Text style={{"color":"black"}}>{this.state.detail}</Text>
                </View>
            </View>
        )
    }

}