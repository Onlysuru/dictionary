import React from 'react';
import { StyleSheet,Text, View } from 'react-native';
import Routes from './src/navigation/route';
import {createAppContainer} from 'react-navigation';

const AppContainer = createAppContainer(Routes);

export default function App() {
  return (
    <View style={{flex: 1}}>
      <AppContainer />
    </View>
  );
}